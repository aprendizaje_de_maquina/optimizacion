# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 22:34:22 2017

@author: Usuario
"""
import sympy as sp
import matplotlib.pyplot as plt
import numpy as np
import time
start_time = time.time()
n=100;
N=n;
a=2; 
b=4;
fold=1;
fnew=1;
x=sp.Symbol('x')
func=8*sp.exp(2-x)+7*sp.log(x-1)
f=[0,1]
q=np.arange(2,4,0.005)
p=8*np.exp(2-q)+7*np.log(q-1)
plt.plot(q,p)
for i in range(2,n+1):
    f.append(f[i-1]+f[i-2])
L2=(b-a)*f[N-3]/f[N-1];
j=2;
while j<N:
    L1=(b-a);
    if L2>L1/2:
        anew=b-L2;
        bnew=a+L2;
    elif L2<=L1/2:
        anew=a+L2;
        bnew=b-L2;   
    k1=func.evalf(subs={x:anew});
    k2=func.evalf(subs={x:bnew});
    plt.plot(anew,k1,'ro')
    plt.plot(bnew,k2,'yo')
    if k2>k1:
        b=bnew;
        L2=f[N-j]*L1/f[N-j+2];
    elif k2<k1:
        a=anew;
        L2=f[N-j]*L1/f[N-(j-2)];
    elif k2==k1:
      b=bnew;
    L2=f[N-j]*(b-a)/f[N-(j-2)];
    j=j+1;        
print(a)
print(b)
print("--- %s segundos, es el tiempo de ejecucion ---" % (time.time() - start_time))