# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 00:40:09 2017

@author: Los Bedoya
"""
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import random
import sympy as sp

def fun(x, y):
  return 10-np.exp(-(x**2+3*y**2))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
xi = yi = np.arange(-1.0, 1.0, 0.05)
X, Y = np.meshgrid(xi, yi)
zs = np.array([fun(xi,yi) for xi,yi in zip(np.ravel(X), np.ravel(Y))])
Z = zs.reshape(X.shape)

ax.plot_surface(X, Y, Z)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()
#import time
#start_time = time.time()
x=sp.Symbol('x')
y=sp.Symbol('y')
x0=1;#valores iniciales
y0=1;#valores iniciales
h=0.00003;
alpha=0.01;
tol=1e-10;
func=10-sp.exp(-(x**2+3*y**2))#funcion
func1=func.evalf(subs={x:x0,y:y0});
func2=func1+1
while abs((func1-func2))>tol:
    func1=func.evalf(subs={x:x0,y:y0});
    gx=((func.evalf(subs={x:h+x0,y:y0}))-(func.evalf(subs={x:x0,y:y0})))/h;
    gy=((func.evalf(subs={x:x0,y:y0+h}))-(func.evalf(subs={x:x0,y:y0})))/h;
    xi=x0-alpha*gx;
    yi=y0-alpha*gy;
    x0=xi;
    y0=yi;
    func2=func.evalf(subs={x:x0,y:y0})
    ax.plot([x0],[y0],[func2],'ro')
    if abs(func2)<abs(func1):
        continue
    else:
        break
print(x0,y0,func2)



