# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 19:21:09 2017

@author: Los Bedoya
"""

import numpy as np
import random
import math
import time  

def jacobo(a):
    start_time = time.time()
    def f(x):
        x1 = x[0]
        x2 = x[1]
        obj=10+x1**2+x2**2-10*(np.cos(2*np.pi*x1)+np.cos(2*np.pi*x2));    
        return obj
    
    x_start = [0,0]
    n = 100;
    m = 100;
    na = 0.0;
    p1 = 0.7;
    p50 = 0.001;
    t1 = -1.0/math.log(p1);
    t50 = -1.0/math.log(p50);
    frac = (t50/t1)**(1.0/(n-1.0));
    x = np.zeros((n+1,2));
    x[0] = x_start;
    xi = np.zeros(2);
    xi = x_start;
    na = na + 1.0;
    xc = np.zeros(2);
    xc = x[0];
    fc = f(xi);
    fs = np.zeros(n+1);
    fs[0] = fc;
    t = t1;
    DeltaE_avg = 0.0;
   
    for i in range(n):
        for j in range(m):
            xi[0] = xc[0] + random.random() - 0.5;
            xi[1] = xc[1] + random.random() - 0.5;
            xi[0] = max(min(xi[0],10.0),-10.0);
            xi[1] = max(min(xi[1],10.0),-10.0);
            DeltaE = abs(f(xi)-fc);
            if (f(xi)>fc):
                if (i==0 and j==0): DeltaE_avg = DeltaE
                p = math.exp(-DeltaE/(DeltaE_avg * t))
                if (random.random()<p):
                    accept = True
                else:
                    accept = False
            else:
                accept = True
            if (accept==True):
                xc[0] = xi[0]
                xc[1] = xi[1]
                fc = f(xc)
                na = na + 1.0
                DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na
        x[i+1][0] = xc[0]
        x[i+1][1] = xc[1]
        fs[i+1] = fc
        t = frac * t
       
    tiempo = time.time() - start_time;
    resultados = [xc[0],xc[1],fc,tiempo] #[xc[0],xc[1],fc,tiempo,fc+10]
    #resultados=np.array(resultadoslista)
   
    return(resultados)