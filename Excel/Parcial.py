# -*- coding: utf-8 -*-

"""
Created on Wed Sep 20 15:29:20 2017

@author: jacobobedoya
"""

from multiprocessing import Pool,cpu_count
import numpy as np
import random
import math
import time  
import xlsxwriter 

def f(a):
    start_time = time.time()
    def f(x):
        #x1 = x[0]
        #x2 = x[1]
        #obj=10+x1**2+x2**2-10*(np.cos(2*np.pi*x1)+np.cos(2*np.pi*x2));
        theta=x[1]*(np.pi/180)
        vely=x[0]*np.sin(theta)
        velx=x[0]*np.cos(theta)
        x1=99;
        y1=2;
        g=9.81;
        ymax=(1/(2*g))*(vely)**2;
        tvuelo=2*(vely/g);
        xymax=(velx)*(1/2)*tvuelo;
        if xymax==0:
            xymax=1000000
        a=-(ymax/xymax**2)
        y=a*(x1-xymax)**2+ymax
        if y<y1:
            xrecorrido=1000000
        else:
            xrecorrido=velx*tvuelo
        if xrecorrido<100:
            xrecorrido=1000000
        
    
        return xrecorrido
        #return obj
    
    x_start = [59,80]
    n = 100;
    m = 100;
    na = 0.0;
    p1 = 0.7;
    p50 = 0.001;
    t1 = -1.0/math.log(p1);
    t50 = -1.0/math.log(p50);
    frac = (t50/t1)**(1.0/(n-1.0));
    x = np.zeros((n+1,2));
    x[0] = x_start;
    xi = np.zeros(2);
    xi = x_start;
    na = na + 1.0;
    xc = np.zeros(2);
    xc = x[0];
    fc = f(xi);
    fs = np.zeros(n+1);
    fs[0] = fc;
    t = t1;
    DeltaE_avg = 0.0;
   
    for i in range(n):
        for j in range(m):
            xi[0] = xc[0] + random.random() - 0.5;
            xi[1] = xc[1] + random.random() - 0.5;
            xi[0] = max(min(xi[0],59),0);
            xi[1] = max(min(xi[1],80),0);
            DeltaE = abs(f(xi)-fc);
            if (f(xi)>fc):
                if (i==0 and j==0): DeltaE_avg = DeltaE
                p = math.exp(-DeltaE/(DeltaE_avg * t))
                if (random.random()<p):
                    accept = True
                else:
                    accept = False
            else:
                accept = True
            if (accept==True):
                xc[0] = xi[0]
                xc[1] = xi[1]
                fc = f(xc)
                na = na + 1.0
                DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na
        x[i+1][0] = xc[0]
        x[i+1][1] = xc[1]
        fs[i+1] = fc
        t = frac * t
       
    tiempo = time.time() - start_time;
    resultados=[fc,xc[0],xc[1],tiempo]
    #resultados = [xc[0],xc[1],fc,tiempo] #[xc[0],xc[1],fc,tiempo,fc+10]
    #resultados=np.array(resultadoslista)
   
    return(resultados)


if __name__ == "__main__":
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    nucleos_del_computador=cpu_count()
    p = Pool(processes=nucleos_del_computador)
    result = p.map(f,range(100))
    a=np.array(result)
    
    workbook = xlsxwriter.Workbook('ParcialRecocidoCaso2.xlsx')
    worksheet = workbook.add_worksheet(name='datos de entrada')
    row_count = 0
    column_count = 0
    table_headers = [
            {'header': 'DistanciaRecorrida'},
            {'header': 'Velocidad'},
            {'header': 'Ángulo'},
            {'header': 'Tiempo'}]
    excel_write_data = a
    table_row_count = row_count + len(excel_write_data)
    table_column_count = column_count + len(table_headers)-1
    table1=worksheet.add_table(row_count, column_count,
                        table_row_count, table_column_count,
                        {'data': excel_write_data,
                         'columns': table_headers})
    #worksheet.write('A1', 'f(x,y)')
    #worksheet.write('B1', 'X')
    #worksheet.write('C1', 'Y')
    #worksheet.write('D1', 'Tiempo')
    worksheet.write('F1', 'Tiempo Total ejecucion')
    worksheet.write_formula('F2', '=_xlfn.SUM(D:D)')
    worksheet.write('F3', 'Desviacion Estandar f(x,y)')
    worksheet.write_formula('F4', '=_xlfn.STDEV.P(A:A)')
    worksheet.write('F5', 'Desviacion Estandar X')
    worksheet.write_formula('F6', '=_xlfn.STDEV.P(B:B)')
    worksheet.write('F7', 'Desviacion Estandar Y')
    worksheet.write_formula('F8', '=_xlfn.STDEV.P(C:C)')
    #worksheet.write('I2', 'f(x,y)')
    #worksheet.write('I3', 'X')
    #worksheet.write('I4', 'Y')
    worksheet.write('J1', 'MINIMO')
    worksheet.write_formula('J2', '=_xlfn.MIN(A:A)')
    #worksheet.write_formula('J3', '=VLOOKUP(J2;table1[#Data];2;FALSE)')
    #worksheet.write_formula('J4', '=VLOOKUP(J2;table1[#Data];3;FALSE)')
    
    workbook.close()