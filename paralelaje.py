# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 15:29:20 2017

@author: jacobobedoya
"""

from multiprocessing import Pool,cpu_count
import numpy as np
import random
import math
import time  
import xlsxwriter as excel

def f(a):
    start_time = time.time()
    def f(x):
        x1 = x[0]
        x2 = x[1]
        obj=10+x1**2+x2**2-10*(np.cos(2*np.pi*x1)+np.cos(2*np.pi*x2));    
        return obj
    
    x_start = [0,0]
    n = 100;
    m = 100;
    na = 0.0;
    p1 = 0.7;
    p50 = 0.001;
    t1 = -1.0/math.log(p1);
    t50 = -1.0/math.log(p50);
    frac = (t50/t1)**(1.0/(n-1.0));
    x = np.zeros((n+1,2));
    x[0] = x_start;
    xi = np.zeros(2);
    xi = x_start;
    na = na + 1.0;
    xc = np.zeros(2);
    xc = x[0];
    fc = f(xi);
    fs = np.zeros(n+1);
    fs[0] = fc;
    t = t1;
    DeltaE_avg = 0.0;
   
    for i in range(n):
        for j in range(m):
            xi[0] = xc[0] + random.random() - 0.5;
            xi[1] = xc[1] + random.random() - 0.5;
            xi[0] = max(min(xi[0],10.0),-10.0);
            xi[1] = max(min(xi[1],10.0),-10.0);
            DeltaE = abs(f(xi)-fc);
            if (f(xi)>fc):
                if (i==0 and j==0): DeltaE_avg = DeltaE
                p = math.exp(-DeltaE/(DeltaE_avg * t))
                if (random.random()<p):
                    accept = True
                else:
                    accept = False
            else:
                accept = True
            if (accept==True):
                xc[0] = xi[0]
                xc[1] = xi[1]
                fc = f(xc)
                na = na + 1.0
                DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na
        x[i+1][0] = xc[0]
        x[i+1][1] = xc[1]
        fs[i+1] = fc
        t = frac * t
       
    tiempo = time.time() - start_time;
    resultados=[fc,xc[0],xc[1],tiempo]
    #resultados = [xc[0],xc[1],fc,tiempo] #[xc[0],xc[1],fc,tiempo,fc+10]
    #resultados=np.array(resultadoslista)
   
    return(resultados)


if __name__ == "__main__":
    nucleos_del_computador=cpu_count()
    p = Pool(processes=nucleos_del_computador)
    result = p.map(f,range(100))#np.array(p.map(f,range(100)))
    #print(result)
    a=np.array(result)
    #Creando El archivo de trabajo
    workbook = excel.Workbook('AnalisisDatos.xlsx')
    #Creando Las Hojas de trabajo, el orden en el que
    #aparezcan las hojas, es el orden en el que serán creadas
    hojadatos = workbook.add_worksheet(name='datos de entrada')
    #columnas y filas donde se empiezan a escribir los datos
    row = 1
    col = 0
    #condicional, para escribir cada elemento en la hoja de excel
    for fxy,x,y,tiempo in (a):
        hojadatos.write(row, col,     fxy)
        hojadatos.write(row, col + 1, y)
        hojadatos.write(row, col + 2, x)
        hojadatos.write(row, col + 3, tiempo)
        row += 1
    #datos que se escriben manualmente, como datos y fórmulas, que
    #se deseeen en el archivo.
    hojadatos.write('A1', 'f(x,y)')
    hojadatos.write('B1', 'Y')
    hojadatos.write('C1', 'X')
    hojadatos.write('D1', 'Tiempo')
    hojadatos.write('F1', 'Tiempo Total ejecucion')
    hojadatos.write_formula('F2', '=_xlfn.SUM(D:D)')
    hojadatos.write('F3', 'Desviacion Estandar f(x,y)')
    hojadatos.write_formula('F4', '=_xlfn.STDEV.P(A:A)')
    hojadatos.write('F5', 'Desviacion Estandar X')
    hojadatos.write_formula('F6', '=_xlfn.STDEV.P(B:B)')
    hojadatos.write('F7', 'Desviacion Estandar Y')
    hojadatos.write_formula('F8', '=_xlfn.STDEV.P(C:C)')
    hojadatos.write('I2', 'f(x,y)')
    hojadatos.write('I3', 'X')
    hojadatos.write('I4', 'Y')
    hojadatos.write('J1', 'MINIMO')
    hojadatos.write_formula('J2', '=_xlfn.MIN(A:A)')
    hojadatos.write_formula('J3', '=VLOOKUP(J2;A:D;2;FALSE)')
    hojadatos.write_formula('J4', '=VLOOKUP(J2;A:D;3;FALSE)')
    
    workbook.close()