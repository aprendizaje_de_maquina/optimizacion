# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 08:46:07 2017

@author: jacobobedoya
"""

#  Se va a trabajar usando la plantilla diseñada por Nathan A. Rooy, en donde
#  el trabaja la optimización PSO a través de clases y objetos, en donde hay un
#  mayor control en el manejo de las variables y del comportamiento del
#  algoritmo.


import random
import numpy as np
#import matplotlib.pyplot as plt

#xi = np.arange(-10.0, 10.0, 0.01)
#yi = np.arange(-10.0, 10.0, 0.01)
#X, Y = np.meshgrid(xi, yi)
#fm = np.zeros(X.shape)
#for i in range(X.shape[0]):
#    for j in range(X.shape[1]):
#        fm[i][j] = 10+X[i][j]**2+Y[i][j]**2-10*(np.cos(2*np.pi*X[i][j])+np.cos(2*np.pi*Y[i][j]));

#plt.figure()
#CS = plt.contour(X, Y, fm)#,lines)
#plt.clabel(CS, inline=1, fontsize=10)
#plt.title('Función')
#plt.xlabel('x1')
#plt.ylabel('x2')


# función que vamos a optimizar (minimo)
def funFitness(x):
    total=10+x[0]**2+x[1]**2-10*(np.cos(2*np.pi*x[0])+np.cos(2*np.pi*x[1]))
    return total

#--- Clase Partícula, Clase PSO -----------------------------------------------

class Particula:
    def __init__(self,x0):
        self.posicion_i=[]           # posicion de la particual
        self.velocidad_i=[]          # velocidad de la particula
        self.pos_mejor_i=[]          # mejor posicion individual
        self.error_mejor_i=-1          # "mejor" error indiviaul
        self.error_i=-1                # error individual

        #asignando velocidad y posición a las partículas
        for i in range(0,dimension):
            self.velocidad_i.append(random.uniform(-1,1))
            self.posicion_i.append(x0[i])

    # Evaluando la función fitnesssssssss
    def evaluar(self,funFitness):
        self.error_i=funFitness(self.posicion_i)

        # posición
        if self.error_i < self.error_mejor_i or self.error_mejor_i==-1:
            self.pos_mejor_i=self.posicion_i
            self.error_mejor_i=self.error_i

    
    def nueva_velocidad(self,pos_mejor_g):
        w=0.75      # peso de inercia
        c1=1.8      # experiencia personal
        c2=2        # experiencia social

        for i in range(0,dimension):
            r1=random.random()
            r2=random.random()
            # actualizando la Nueva velocidad de la partícula
            velpersonal=c1*r1*(self.pos_mejor_i[i]-self.posicion_i[i])
            velsocial=c2*r2*(pos_mejor_g[i]-self.posicion_i[i])
            self.velocidad_i[i]=w*self.velocidad_i[i]+velsocial+velpersonal

    
    def nueva_posicion(self,limites):
        for i in range(0,dimension):
            self.posicion_i[i]=self.posicion_i[i]+self.velocidad_i[i]

            # ajustando maxima posición en caso de ser necesario ¿?
            if self.posicion_i[i]>limites[i][1]:
                self.posicion_i[i]=limites[i][1]

            # ajustando mínima posición en caso de ser necesario ¿?
            if self.posicion_i[i] < limites[i][0]:
                self.posicion_i[i]=limites[i][0]
                
def PSO(funFitness,x0,limites,num_particulas,iteracionesmax):
    global dimension
    dimension=len(x0)
    error_mejor_g=-1                   # best error for group
    pos_mejor_g=[]                   # best position for group

        # creando las particulas
    swarm=[]
    for i in range(0,num_particulas):
        swarm.append(Particula(x0))
        
    i=0
    while i < iteracionesmax:
        for j in range(0,num_particulas):
            swarm[j].evaluar(funFitness)
            if swarm[j].error_i < error_mejor_g or error_mejor_g == -1:
                pos_mejor_g=list(swarm[j].posicion_i)
                error_mejor_g=float(swarm[j].error_i)

           
        for j in range(0,num_particulas):
            swarm[j].nueva_velocidad(pos_mejor_g)
            swarm[j].nueva_posicion(limites)
                
        i+=1

# print final results

        #print (pos_mejor_g)
        #print (error_mejor_g)
    resultados= (pos_mejor_g[0],pos_mejor_g[1], error_mejor_g)
        #plt.plot(pos_mejor_g[0],pos_mejor_g[1],'ro')
    return(resultados)



#----Evaluación----------------------------------------------------------------

posicionINICIAL=[6,4]               
limites=[(-10,10),(-10,10)]
a=PSO(funFitness,x0=[6,4],limites=[(-10,10),(-10,10)],num_particulas=100,iteracionesmax=100)
