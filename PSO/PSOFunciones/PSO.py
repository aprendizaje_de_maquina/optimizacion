# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 19:35:18 2017

@author: Jacobo
"""

from multiprocessing import Pool,cpu_count
import numpy as np
from funcPSO import PSO
#import xlsxwriter 
import time  
def f(a):
    #x[0] es la velocidad inicial x[1] es el ángulo
    start_time = time.time()
    def funFitness(x):
        theta=x[1]*(np.pi/180)
        vely=x[0]*np.sin(theta)
        velx=x[0]*np.cos(theta)
        x1=99
        y1=2
        g=9.81
        ymax=(1/(2*g))*(vely)**2
        tvuelo=2*(vely/g)
        xymax=(velx)*(1/2)*tvuelo
        if xymax==0:
            xymax=1000000
        a=-(ymax/xymax**2)
        y=a*(x1-xymax)**2+ymax
        if y<y1:
            xrecorrido=1000000
        else:
            xrecorrido=velx*tvuelo
        if xrecorrido<100:
            xrecorrido=1000000
        return xrecorrido
    posicionINICIAL=[59,80]  #velocidad inicial y ángulo              
    limites=[(0,59),(0,80)]  #rangos de velocidades[m/s] y rangos de angulos[radianes]
    distancia=np.asarray(PSO(funFitness,posicionINICIAL,limites,num_particulas=100,iteracionesmax=100))
    tiempo = time.time() - start_time
    resultados=(distancia[0],distancia[1],distancia[2],tiempo)
    return(resultados)

if __name__ == "__main__":
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    nucleos_del_computador=cpu_count()
    p = Pool(processes=nucleos_del_computador)
    result = p.map(f,range(100))
    a=np.array(result)
    print(a)
    
    #workbook = xlsxwriter.Workbook('ParcialPSOCaso2.xlsx')
    #worksheet = workbook.add_worksheet(name='datos de entrada')
    #row_count = 0
    #column_count = 0
    #table_headers = [
    #        {'header': 'Velocidad'},
    #        {'header': 'DistanciaRecorrida'},
    #        {'header': 'Tiempo'}]
    #        {'header': 'Ángulo'},
    #excel_write_data = a
    #table_row_count = row_count + len(excel_write_data)
    #table_column_count = column_count + len(table_headers)-1
    #table1=worksheet.add_table(row_count, column_count,
    #                    table_row_count, table_column_count,
    #                    {'data': excel_write_data,
    #                     'columns': table_headers})
    #worksheet.write('A1', 'f(x,y)')
    #worksheet.write('B1', 'X')
    #worksheet.write('C1', 'Y')
    #worksheet.write('D1', 'Tiempo')
    #worksheet.write('F1', 'Tiempo Total ejecucion')
    #worksheet.write_formula('F2', '=_xlfn.SUM(D:D)')
    #worksheet.write('F3', 'Desviacion Estandar f(x,y)')
    #worksheet.write_formula('F4', '=_xlfn.STDEV.P(A:A)')
    #worksheet.write('F5', 'Desviacion Estandar X')
    #worksheet.write_formula('F6', '=_xlfn.STDEV.P(B:B)')
    #worksheet.write('F7', 'Desviacion Estandar Y')
    #worksheet.write_formula('F8', '=_xlfn.STDEV.P(C:C)')
    #worksheet.write('I2', 'f(x,y)')
    #worksheet.write('I3', 'X')
    #worksheet.write('I4', 'Y')
    #worksheet.write('J1', 'MINIMO')
    #worksheet.write_formula('J2', '=_xlfn.MIN(C:C)')
    #worksheet.write_formula('J3', '=VLOOKUP(J2;table1[#Data];2;FALSE)')
    #worksheet.write_formula('J4', '=VLOOKUP(J2;table1[#Data];3;FALSE)')
    
    #workbook.close()