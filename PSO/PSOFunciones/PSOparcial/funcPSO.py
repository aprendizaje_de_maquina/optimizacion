# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 18:34:43 2017

@author: Jacobo
"""
import random
class Particula:
    def __init__(self,x0):
        self.posicion_i=[]           # posicion de la particual
        self.velocidad_i=[]          # velocidad de la particula
        self.pos_mejor_i=[]          # mejor posicion individual
        self.error_mejor_i=-1          # "mejor" error indiviaul
        self.error_i=-1                # error individual

        #asignando velocidad y posición a las partículas
        for i in range(0,dimension):
            self.velocidad_i.append(random.uniform(-1,1))
            self.posicion_i.append(x0[i])

    # Evaluando la función fitnesssssssss
    def evaluar(self,funFitness):
        self.error_i=funFitness(self.posicion_i)

        # posición
        if self.error_i < self.error_mejor_i or self.error_mejor_i==-1:
            self.pos_mejor_i=self.posicion_i
            self.error_mejor_i=self.error_i

    
    def nueva_velocidad(self,pos_mejor_g):
        w=0.75      # peso de inercia
        c1=1.8      # experiencia personal
        c2=2        # experiencia social

        for i in range(0,dimension):
            r1=random.random()
            r2=random.random()
            # actualizando la Nueva velocidad de la partícula
            velpersonal=c1*r1*(self.pos_mejor_i[i]-self.posicion_i[i])
            velsocial=c2*r2*(pos_mejor_g[i]-self.posicion_i[i])
            self.velocidad_i[i]=w*self.velocidad_i[i]+velsocial+velpersonal

    
    def nueva_posicion(self,limites):
        for i in range(0,dimension):
            self.posicion_i[i]=self.posicion_i[i]+self.velocidad_i[i]

            # ajustando maxima posición en caso de ser necesario ¿?
            if self.posicion_i[i]>limites[i][1]:
                self.posicion_i[i]=limites[i][1]

            # ajustando mínima posición en caso de ser necesario ¿?
            if self.posicion_i[i] < limites[i][0]:
                self.posicion_i[i]=limites[i][0]
                
def PSO(funFitness,x0,limites,num_particulas,iteracionesmax):
    global dimension
    dimension=len(x0)
    error_mejor_g=-1                   # Mejor error del grupo
    pos_mejor_g=[]                   # Mejor posición del grupo

        # creando las partículas
    swarm=[]
    for i in range(0,num_particulas):
        swarm.append(Particula(x0))
        
    i=0
    while i < iteracionesmax:
        for j in range(0,num_particulas):
            swarm[j].evaluar(funFitness)
            if swarm[j].error_i < error_mejor_g or error_mejor_g == -1:
                pos_mejor_g=list(swarm[j].posicion_i)
                error_mejor_g=float(swarm[j].error_i)

           
        for j in range(0,num_particulas):
            swarm[j].nueva_velocidad(pos_mejor_g)
            swarm[j].nueva_posicion(limites)
                
        i+=1

    resultados= (pos_mejor_g[0],pos_mejor_g[1], error_mejor_g)
        
    return(resultados)