# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 15:10:44 2017

@author: jacobobedoya
"""

from ClasePSO import PSO
import numpy as np

def funFitness(x):
    total=10+x[0]**2+x[1]**2-10*(np.cos(2*np.pi*x[0])+np.cos(2*np.pi*x[1]))
    return total

posicionINICIAL=[6,4]               
limites=[(-10,10),(-10,10)]
PSO(funFitness,posicionINICIAL,limites,num_particulas=100,iteracionesmax=100)