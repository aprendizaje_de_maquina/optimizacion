# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 15:07:43 2017

@author: jacobobedoya
"""
import random

class Particula:
    
    #dimension=2 #como no esta guardada globalmente se tiene que redefinir por
                #problema
    def __init__(self,x0,dimension):
        
        #dimension=len(x0)
        self.posicion_i=[]            # posicion de la particual
        self.velocidad_i=[]           # velocidad de la particula
        self.pos_mejor_i=[]           # mejor posicion individual
        self.error_mejor_i=-1         # "mejor" error indiviaul
        self.error_i=-1               # error individual
        

        #asignando velocidad y posición a las partículas
        for i in range(0,len(x0)):
            self.velocidad_i.append(random.uniform(-1,1))
            self.posicion_i.append(x0[i])

    # Evaluando la función fitnesssssssss
    def evaluate(self,funFitness):
        self.error_i=funFitness(self.posicion_i)

        # posición
        if self.error_i < self.error_mejor_i or self.error_mejor_i==-1:
            self.pos_mejor_i=self.posicion_i
            self.error_mejor_i=self.error_i

    
    def nueva_velocidad(self,pos_mejor_g):
        w=0.75      # peso de inercia
        c1=1.8      # experiencia personal
        c2=2        # experiencia social

        for i in range(0,len(x0)):
            r1=random.random()
            r2=random.random()
            # actualizando la Nueva velocidad de la partícula
            velpersonal=c1*r1*(self.pos_mejor_i[i]-self.posicion_i[i])
            velsocial=c2*r2*(pos_mejor_g[i]-self.posicion_i[i])
            self.velocidad_i[i]=w*self.velocidad_i[i]+velsocial+velpersonal

    
    def nueva_posicion(self,limites):
        for i in range(0,len(x0)):
            self.posicion_i[i]=self.posicion_i[i]+self.velocidad_i[i]

            # ajustando maxima posición en caso de ser necesario ¿?
            if self.posicion_i[i]>limites[i][1]:
                self.posicion_i[i]=limites[i][1]

            # ajustando mínima posición en caso de ser necesario ¿?
            if self.posicion_i[i] < limites[i][0]:
                self.posicion_i[i]=limites[i][0]
                