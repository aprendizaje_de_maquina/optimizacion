# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 15:09:51 2017

@author: jacobobedoya
"""
from ClaseParticula import Particula

class PSO():
    def __init__(self,funFitness,x0,limites,num_particulas,iteracionesmax):
        global dimension

        dimension=len(x0)
        error_mejor_g=-1                  
        pos_mejor_g=[]                  

        # creando las particulas
        swarm=[]
        for i in range(0,num_particulas):
            swarm.append(Particula(x0,dimension))

        i=0
        while i < iteracionesmax:
            #evaluando las particulas en la función fitnessssssss
            for j in range(0,num_particulas):
                swarm[j].evaluate(funFitness)

                #comprobando los globales
                if swarm[j].error_i < error_mejor_g or error_mejor_g == -1:
                    pos_mejor_g=list(swarm[j].posicion_i)
                    error_mejor_g=float(swarm[j].error_i)

            #nuevas posiciones y velocidades
            for j in range(0,num_particulas):
                swarm[j].nueva_velocidad(pos_mejor_g)
                swarm[j].nueva_posicion(limites)
            i+=1

        # print final results
        print ('FINAL:')
        #print (pos_mejor_g)
        #print (error_mejor_g)
        resultados= (pos_mejor_g[0],pos_mejor_g[1], error_mejor_g)
        print(resultados)