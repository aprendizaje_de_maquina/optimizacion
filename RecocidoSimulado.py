## Plot de contorno

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import random
import math

# Funcion Objetivo
def f(x):
    #x1 = x[0]
    #x2 = x[1]
    theta=x[1]*(np.pi/180)
    vely=x[0]*np.sin(theta)
    velx=x[0]*np.cos(theta)
    x1=99;
    y1=10;
    g=9.81;
    ymax=(1/(2*g))*(vely)**2;
    tvuelo=2*(vely/g);
    xymax=(velx)*(1/2)*tvuelo;
    if xymax==0:
        xymax=1000000
    a=-(ymax/xymax**2)
    y=a*(x1-xymax)**2+ymax
    if y<y1:
        xrecorrido=1000000
    else:
        xrecorrido=velx*tvuelo
    
    
    if xrecorrido<100:
        xrecorrido=1000000
        
    
    return xrecorrido
    
    #obj=10+x1**2+x2**2-10*(np.cos(2*np.pi*x1)+np.cos(2*np.pi*x2));
    
    #return obj

#Valores de inicio
x_start = [53,80]

#Volviendo los valores vectores para los la figura de contorno
#i1 = np.arange(-10.0, 10.0, 0.01)
#i2 = np.arange(-10.0, 10.0, 0.01)
#x1m, x2m = np.meshgrid(i1, i2)
#fm = np.zeros(x1m.shape)
#for i in range(x1m.shape[0]):
#    for j in range(x1m.shape[1]):
#        #fm[i][j] = (-np.cos(x1m[i][j])*np.cos(x2m[i][j])*np.exp(-(((x1m[i][j])-np.pi)**2+((x2m[i][j])-np.pi)**2)));
#        fm[i][j] = 10+(x1m[i][j])**2+(x2m[i][j])**2-10*(np.cos(2*np.pi*(x1m[i][j]))+np.cos(2*np.pi*(x2m[i][j])))
        
#plt.figure()
#CS = plt.contour(x1m,x2m, fm)#,lines)
#plt.clabel(CS, inline=1, fontsize=10)
#plt.title('Función')
#plt.xlabel('x1')
#plt.ylabel('x2')


import time
start_time = time.time()   
##################################################
# Recocido Simulado
##################################################
# Numero de cliclos
n = 100;
# Numero de intentos por ciclo
m = 100;
# Soluciones aceptadas
na = 0.0;
# Probabilidad de aceptar la peor solucion al inicio
p1 = 0.7;
# Probabilidad de aceptar la peor solucion al final
p50 = 0.001;
# Temperatura inicial
t1 = -1.0/math.log(p1);
# temperatura final
t50 = -1.0/math.log(p50);
# Reducción por ciclo (fraccionaria)
frac = (t50/t1)**(1.0/(n-1.0));
# Iniciando el vector de respuestas x
x = np.zeros((n+1,2));
x[0] = x_start;
xi = np.zeros(2);
xi = x_start;
na = na + 1.0;
# Mejores resultados hasta el momento
xc = np.zeros(2);
xc = x[0];
fc = f(xi);
fs = np.zeros(n+1);
fs[0] = fc;
# Temperatura
t = t1;
# DeltaE Promedio
DeltaE_avg = 0.0;
jacobo=0;
for i in range(n):
    #print('Cilo: ' + str(i) + ' Con Temperatura: ' + str(t))
    for j in range(m):
        # Opcion de nuevo intento
        xi[0] = xc[0] + random.random() - 0.5;
        xi[1] = xc[1] + random.random() - 0.5;
        # limites superiores e inferiores
        xi[0] = max(min(xi[0],56),0);
        xi[1] = max(min(xi[1],80),0
          
          );
        DeltaE = abs(f(xi)-fc);
        if (f(xi)>fc):
            # Iniciar DeltaE_avg si la peor solucion fue encontrada
            #   en la primera iteracion
            if (i==0 and j==0): DeltaE_avg = DeltaE
            # Funcion Objetivo es peor
            # generar probabilidad de aceptacion
            p = math.exp(-DeltaE/(DeltaE_avg * t))
            # Determinar si se acepta o no el peor punto
            if (random.random()<p):
                # Aceptar la peor solucion
                accept = True
            else:
                # No aceptar la peor solucion
                accept = False
        else:
            # Si la funcion Objetivo es menor, aceptar automaticamente
            accept = True
        if (accept==True):
            # Actualizar la solucion aceptada
            xc[0] = xi[0]
            xc[1] = xi[1]
            fc = f(xc)
            # incrementar el numero de soluciones aceptadas
            na = na + 1.0
            # Actualizar DeltaE_avg
            DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na
    # Guardar los mejores valores por cada ciclo
    x[i+1][0] = xc[0]
    x[i+1][1] = xc[1]
    fs[i+1] = fc
    # Bajar la temperatura para el ciclo siguiente
    t = frac * t

# Solucion
print("--- %s segundos, es el tiempo de ejecucion ---" % (time.time() - start_time))
print('Best solution: ' + str(xc))
print('Best objective: ' + str(fc))


#plt.plot(x[:,0],x[:,1],'y-o')
#plt.savefig('contour.png')

#fig = plt.figure()
#ax1 = fig.add_subplot(211)
#ax1.plot(fs,'r.-')
#ax1.legend(['Objective'])
#ax2 = fig.add_subplot(212)
#ax2.plot(x[:,0],'b.-')
#ax2.plot(x[:,1],'g--')
#ax2.legend(['X','Y'])


