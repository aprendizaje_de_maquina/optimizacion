# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 00:40:09 2017

@author: Los Bedoya
"""
import sympy as sp
import matplotlib.pyplot as plt
import numpy as np
import time
start_time = time.time()
x=sp.Symbol('x')
func=x**4-14*x**3+60*x**2-70*x
q=np.arange(-4,4,0.005)
p=q**4-14*q**3+60*q**2-70*q
plt.plot(q,p)
tol=1e-16;
gr=(5**0.5+1)/2
a=-4; 
b=4;
c=b-(b-a)/gr;
d=a+(b-a)/gr;
while abs(c-d)>tol:
    k1=func.evalf(subs={x:c});
    k2=func.evalf(subs={x:d});
    plt.plot(c,k1,'ro')
    plt.plot(d,k2,'yo')
    if k1<k2:
        b=d
    else:
        a=c
        # we recompute both c and d here to avoid loss of precision which may lead to incorrect results or infinite loop
    c=b-(b-a)/gr
    d=a+(b-a)/gr
print('a=',a)
print('b=',b)
print("--- %s segundos, es el tiempo de ejecucion ---" % (time.time() - start_time))
