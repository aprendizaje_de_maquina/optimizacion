# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 11:12:18 2017

@author: jacobobedoya
"""
from parcial3 import jacobo
from multiprocessing import Pool,cpu_count
import numpy as np

if __name__ == "__main__":
    nucleos_del_computador=cpu_count()
    p = Pool(processes=nucleos_del_computador)
    result = np.array(p.map(jacobo,[1,2,3,4,5]))
    print(result)
