# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 22:41:48 2017

@author: Family
"""

from multiprocessing import Pool,cpu_count
from parcial3 import jacobo
from GeneticoJacobo import jacobo_genetico
import numpy as np
import xlsxwriter as excel

#Para 

#todo lo del código debe estar indentado debajo del if, ya que,
#si no se hace, como el codigo nunca sale del condicional, nunca
#va a correr
if __name__ == "__main__":
    nucleos_del_computador=cpu_count()
    p = Pool(processes=nucleos_del_computador)
    result1 = np.array(p.map(jacobo,range(100)))
    result2 = np.array(p.map(jacobo_genetico,range(100)))
    
    #Creando El archivo de trabajo
    workbook = excel.Workbook('AnalisisMetodos.xlsx')
    #Creando Las Hojas de trabajo, el orden en el que
    #aparezcan las hojas, es el orden en el que serán creadas
    hojadatos1 = workbook.add_worksheet(name='datos de entrada recocido simulado')
    hojadatos2 = workbook.add_worksheet(name='datos de entrada algoritmo genetico')
    #columnas y filas donde se empiezan a escribir los datos
    row = 1
    col = 0
    #condicional, para escribir cada elemento en la hoja de excel
    for x,y,fxy,tiempo in (result1):
        hojadatos1.write(row, col,     x)
        hojadatos1.write(row, col + 1, y)
        hojadatos1.write(row, col + 2, fxy)
        hojadatos1.write(row, col + 3, tiempo)
        row += 1
    #datos que se escriben manualmente, como datos y fórmulas, que
    #se deseeen en el archivo.
    hojadatos1.write(0, 0, 'X')
    hojadatos1.write(0, 1, 'Y')
    hojadatos1.write(0, 2, 'f(x,y)')
    hojadatos1.write(0, 3, 'Tiempo')
    workbook.close()

