clc;clear;close all
tic
%Para hallar los puntos de inflección
% syms x
% f= x^4-14*x^3+60*x^2-70*x;
% ezplot(f);
% % 
% a=solve(diff(f,x),x);
% disp a
% 
% b(1)=subs(diff(f,x,3),a(1));
% b(2)=subs(diff(f,x,3),a(2));
% 
% if b(1)~=0
%     a(1)
%     disp 'punto de inflección'
% else
%     disp 'no es punto de inflección'
% end
% if b(2)~=0
%     a(2)
%     disp 'punto de inflección'
% else
%     disp 'no es punto de inflección'
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n=1000;
N=n+1;
a=-10; 
b=4;
fold=1;
fnew=1;
func = @(x)(x^4-14*x^3+60*x^2-70*x);
for i=1:N
    if i==1 || i==2
    f(i)=1;
   continue;
    end
    f(i)=fold+fnew;
    fold=fnew;
    fnew=f(i);
end
L2=(b-a)*f(N-2)/f(N);
j=2;
while j<N
L1=(b-a);
if L2>L1/2
    anew=b-L2;
    bnew=a+L2;
else if L2<=L1/2
        anew=a+L2;
        bnew=b-L2;
    end
end
k1=func(anew);
k2=func(bnew);
if k2>k1
    b=bnew;
    L2=f(N-j)*L1/f(N-j+2);
else if k2<k1
        a=anew;
        L2=f(N-j)*L1/f(N-(j-2));
    else if k2==k1
            b=bnew;
            L2=f(N-j)*(b-a)/f(N-(j-2));
            j=j+1;
        end
    end
end
j=j+1;
end
disp(a);
disp(b);
toc
    
    
    
    
