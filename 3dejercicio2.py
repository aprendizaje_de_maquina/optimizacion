# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 16:27:58 2017

@author: Los Bedoya
"""

import numpy as np
import matplotlib.pyplot as plt
import sympy as sp

def fun(x, y):
  return (-np.cos(x)*np.cos(y)*np.exp(-((x-np.pi)**2+(y-np.pi)**2)))

xi = np.arange(1.0, 5.0, 0.01)
yi = np.arange(1.0, 5.0, 0.01)
X, Y = np.meshgrid(xi, yi)
fm = np.zeros(X.shape)
for i in range(X.shape[0]):
    for j in range(X.shape[1]):
        fm[i][j] = (-np.cos(X[i][j])*np.cos(Y[i][j])*np.exp(-(((X[i][j])-np.pi)**2+((Y[i][j])-np.pi)**2)));


plt.figure()
CS = plt.contour(X, Y, fm)#,lines)
plt.clabel(CS, inline=1, fontsize=10)
plt.title('Función')
plt.xlabel('x1')
plt.ylabel('x2')

import time
start_time = time.time()
x=sp.Symbol('x')
y=sp.Symbol('y')
x0=3;#valores iniciales
y0=3;#valores iniciales
h=0.003;
alpha=0.01;
tol=1e-3;
func=(-sp.cos(x)*sp.cos(y)*sp.exp(-((x-sp.pi)**2+(y-sp.pi)**2)))#funcion
func1=0
func2=1
while abs((func1-func2))>tol:
    func1=func.evalf(subs={x:x0,y:y0});
    gx=((func.evalf(subs={x:h+x0,y:y0}))-(func.evalf(subs={x:x0,y:y0})))/h;
    gy=((func.evalf(subs={x:x0,y:y0+h}))-(func.evalf(subs={x:x0,y:y0})))/h;
    xi=x0-alpha*gx;
    yi=y0-alpha*gy;
    x0=xi;
    y0=yi;
    func2=func.evalf(subs={x:x0,y:y0})
    plt.plot(x0,y0,'ro')
    if func2<func1:
        continue
    else:
        break
print("--- %s segundos, es el tiempo de ejecucion ---" % (time.time() - start_time))
print(x0,y0,func2)
