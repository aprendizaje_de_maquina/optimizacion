# -*- coding: utf-8 -*-
"""
Created on Tue Sep  2 13:49:22 2014

@author: Hossein Abedi 
"""

'''
Requirements: Python v2.7.6 or 2.7.5
              Numpy v1.8 or newer

A class for basic implementation of 3 nature inspyred algorithm  for finding the maximum of f(.)

1-Particle Swarm Optimization (PSO) -(Without limiting the velocies)
2-Differential Evolution (DE)
3-Cuckoo Search algorithm (CS)
'''

import numpy as np

class Particle:
    ''' This class contains the code of the Particles in the swarm'''
    def __init__(self,dimension,popsize,ul):
        self.dimension=dimension
        self.popsize=popsize
        self.ul=ul
        pop=np.random.rand(self.popsize,self.dimension)*(self.ul[1]-self.ul[0])+np.ones([self.popsize,self.dimension])*self.ul[0]     
        self.velocity=np.random.rand(self.popsize,self.dimension)
        self.pos=pop
        self.pBest=self.pos
        
    def updatePos(self):
           self.pos = np.add(self.pos, self.velocity)  
           
    def updateVeloc(self, gBest,c1,c2,w):
            
            r1 = np.random.random()
            r2 = np.random.random()
            social = np.multiply(c1 ,np.multiply( r1 , (np.subtract(np.tile(gBest,[self.popsize,1]) , self.pos))))
            cognitive = np.multiply(c2 ,np.multiply(r2 , np.subtract(self.pBest , self.pos)))
            
            self.velocity = np.add(np.add(np.multiply(w ,self.velocity), social) , cognitive)
            
class ParticleSwarmOptimizer():
 
    def __init__(self,c1=2.05,c2=2.05,w=.9,Ite=3000,dim=2,popsize=30,ul=[-100,100],f=None):
            self.c1=c1
            self.c2=c2
            self.w=w
            self.f=f
            self.Ite=Ite
            self.dim=dim
            self.popsize=popsize
            self.ul=ul
            self.pop= Particle(self.dim,self.popsize,self.ul)
            self.gBest = self.pop.pos[0]
            return
 
    def optimize(self):
        fpBest=np.ones([self.popsize])
        for i in range(0,self.Ite,2*self.popsize):
            print "iteration ", i,self.f(self.gBest)
            #Get the global best particle
            gb=self.f(self.gBest)
            for j in range(self.popsize):
                pBest = self.pop.pBest[j]
                fpBest[j]=self.f(pBest)
                if fpBest[j] < gb:
                    self.gBest = pBest 
                    gb=fpBest[j]
                    
            
            #Update position of each paricle
            self.pop.updateVeloc(self.gBest,self.c1,self.c2,self.w)
            self.pop.updatePos()
            I= self.pop.pos<self.ul[0]
            self.pop.pos[I]=self.ul[0]
            I= self.pop.pos>self.ul[1]
            self.pop.pos[I]=self.ul[1]
            #Update the personal best positions
            for l in range(self.popsize):
                pBest= self.pop.pBest[l]
                if self.f(self.pop.pos[l]) < fpBest[l]:
                    self.pop.pBest[l] = self.pop.pos[l]
         
class CuckooSearchOptimizer():
    '''Cuckoo Search OPtimizer written in Python 2.7.6 '''
    def __init__(self,p_d=.25,Ite=3000,dim=2,n_nest=20,ul=[-100,100],f=None):
            '''Initilal parameters'''
            self.n_nest=n_nest
            self.ul=ul
            self.dim=dim
            self.beta=1.5
            self.Ite=Ite
            self.p_d=.25
            self.sigma=0.69657450255769682
            self.f=f
            self.nests=np.array([np.array([np.random.random()*(self.ul[1]-self.ul[0])+self.ul[0] for j in xrange(dim)],dtype=np.float64) for i in xrange(self.n_nest)])
            self.best=self.nests[0,:]
            self.f_best=self.f(self.best);
            for i in xrange(1,self.n_nest):
               f_nest=self.f(self.nests[i])
               if f_nest<self.f_best:
                   self.best=self.nests[i][:]
                   self.f_best=f_nest
            
    def optimize(self):
        
        count=0
        print self.f_best," Iteration",count 
        while (count<self.Ite):  
           for n in xrange(self.n_nest):
               
                        u=np.multiply(np.random.randn(self.dim),self.sigma)
                        v=np.random.randn(self.dim)
                        step=np.divide(u,np.power(np.abs(v),np.divide(1.,self.beta)))
    
    
                        stepsize=np.multiply(.01,np.multiply(step,np.subtract(self.best,self.nests[n])))
                        kat=(np.add(self.nests[n],np.multiply(stepsize,np.random.randn(self.dim))))[:]
                        for m in xrange(self.dim):                   
                              if kat[m]<self.ul[0]:
                                  kat[m]=self.ul[0]
                              elif kat[m]>self.ul[1]:
                                      kat[m]=self.ul[1] 
                        
                        self.f_nest=self.f(self.nests[n])
                        f_kat=self.f(kat);
                        if f_kat<self.f_nest:
                                 self.nests[n]=kat.copy()
                        
           
           for i in xrange(self.n_nest):
             f_nest=self.f(self.nests[i])
             if f_nest<self.f_best:
                self.best=self.nests[i][:]
                self.f_best=f_nest
               
          
                     
           count=np.add(count,np.multiply(2,self.n_nest))
           
           print self.f_best," Iteration",count 
           
           K=np.array([np.random.random([self.n_nest,self.dim])>self.p_d],dtype=int)
           stepsize=np.multiply(np.subtract(self.nests[np.random.permutation(self.n_nest)],self.nests[np.random.permutation(self.n_nest)]),np.multiply(.01,np.random.random()))
          
           self.nests=np.add(self.nests,np.multiply(stepsize,K[0])).copy()
           for  n in xrange(self.n_nest):
                 for m in xrange(self.dim):                   
                              if self.nests[n][m]<self.ul[0]:
                                  self.nests[n][m]=self.ul[0]
                              if self.nests[n][m]>self.ul[1]:
                                      self.nests[n][m]=self.ul[1]
           for i in xrange(self.n_nest):
             f_nest=self.f(self.nests[i])
             if f_nest<self.f_best:
                self.best=self.nests[i][:]
                self.f_best=f_nest
           
           count=np.add(count,np.multiply(1,self.n_nest))
           print self.f_best," Iteration",count 


class DiffrentialEvolutionOptimizer():
    '''Differential Evolution Optimizer written in Python 2.7.6 '''
    def __init__(self,CR=.25,F=1.5,Ite=3000,dim=2,popsize=20,ul=[-100,100],f=None):
         self.CR=CR
         self.F=F
         self.Ite=Ite
         self.popsize=popsize
         self.ul=ul
         self.dim=dim
         self.f=f
         self.pop=np.random.rand(self.popsize,self.dim)*(self.ul[1]-self.ul[0])+np.ones([self.popsize,self.dim])*self.ul[0]  
     
    def optimize(self):
        
         self.best=self.pop[0,:]
         self.f_best=self.f(self.best);
         for i in xrange(1,self.popsize):
               f_can=self.f(self.pop[i,:])
               if f_can<self.f_best:
                   self.best=self.pop[i][:]
                   self.f_best=f_can
        
         count=self.popsize+1
         while (count<self.Ite):
              for i in xrange(self.popsize):
                  tempS=set(np.arange(self.popsize))
                  t0=np.random.randint(self.popsize)
                  a=self.pop[t0,:]
                  t1=np.random.choice(list(tempS.difference([t0])))
                  t2=np.random.choice(list(tempS.difference([t0,t1])))
                  
                  b=self.pop[t1,:]
                  c=self.pop[t2,:]
                  
                  #for j in xrange(self.dim):
                  r=np.random.rand(self.dim)
                  t=r<self.CR
                  y=np.zeros([self.dim])
                  y[t]=np.add(a[t],np.multiply(np.subtract(b[t],c[t]),self.F))
                  
                  I=y<self.ul[0]
                  y[I]=self.ul[0]
                  I=y>self.ul[1]
                  y[I]=self.ul[1]
                  
                  if self.f(y)<self.f(self.pop[i,:]):
                      self.pop[i,:]=y[:]
             
              for i in xrange(1,self.popsize):
                  f_can=self.f(self.pop[i,:])
                  if f_can<self.f_best:
                     self.best=self.pop[i][:]
                     self.f_best=f_can
              count=np.add(count,np.multiply(3,self.popsize))
              print self.f_best,"Iteration = ",count
                      
                      
                      
                   

            