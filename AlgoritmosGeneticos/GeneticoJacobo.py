# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 20:35:44 2017

@author: Family
"""

import operator
import random
import numpy as np
from scipy import interpolate
from deap import base
#from deap import benchmarks
from deap import creator
from deap import tools
from multiprocessing import Pool,cpu_count
import time 
import xlsxwriter 

def jacobo_genetico(a):
    start_time = time.time()
    t1=np.linspace(-10,10,1000)
    t2=np.linspace(-10,10,1000)
    sig1=10+t1**2+t2**2-10*(np.cos(2*np.pi*t1)+np.cos(2*np.pi*t2))
    sig2=10+t1**2+t2**2-10*(np.cos(2*np.pi*t1)+np.cos(2*np.pi*t2))
    f0=interpolate.interp1d(t1,sig1,kind="cubic",bounds_error=False,fill_value=-10000)
    g=interpolate.interp1d(t1,sig2,kind="cubic",bounds_error=False,fill_value=10000)
    #true value that  I would like to estimate
    A=10
    B=10
    C=10
    def s(t):
        return A+B*t+C*t*t
    def inv_s(t):
        return (t-B)/A
    def f(t):
        return f0(s(t))
    def hat_s(t,a,b):
        return t*b+a
    Ig=np.arange(t1.min(),t1.max(),1)
    If=(Ig-A)/B
    I_min=max(If.min(),Ig.min())
    I_max=min(If.max(),Ig.max())
    
    #J=np.linspace(min(If.min(),Ig.min()),max(If.max(),Ig.max()))
    I=np.linspace(I_min,I_max,1000)  
    def cost(x,T=I):
        a=x[0]
        b=x[1]
        return (np.linalg.norm(g(b*T+a)-f(T))/len(T),)
    creator.create("FitnessMax", base.Fitness, weights=(-1.0,))
    creator.create("Particle", list, fitness=creator.FitnessMax, speed=list, 
                   smin=None, smax=None, best=None)
    def generate(size, pmin, pmax, smin, smax):
        part = creator.Particle(random.uniform(pmin, pmax) for _ in range(size)) 
        part.speed = [random.uniform(smin, smax) for _ in range(size)]
        part.smin = smin
        part.smax = smax
        return (part)
    def updateParticle(part, best, phi1, phi2):
        u1 = (random.uniform(0, phi1) for _ in range(len(part)))
        u2 = (random.uniform(0, phi2) for _ in range(len(part)))
        v_u1 = map(operator.mul, u1, map(operator.sub, part.best, part))
        v_u2 = map(operator.mul, u2, map(operator.sub, best, part))
        part.speed = list(map(operator.add, part.speed, map(operator.add, v_u1, v_u2)))
        for i, speed in enumerate(part.speed):
            if speed < part.smin:
                part.speed[i] = part.smin
            elif speed > part.smax:
                part.speed[i] = part.smax
        part[:] = list(map(operator.add, part, part.speed))
    toolbox = base.Toolbox()
    toolbox.register("particle", generate, size=2, pmin=-6, pmax=6, smin=-3, smax=3)
    toolbox.register("population", tools.initRepeat, list, toolbox.particle)
    toolbox.register("update", updateParticle, phi1=2.0, phi2=2.0)
    toolbox.register("evaluate", cost)
    def main():
        pop = toolbox.population(n=5)
        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)
                
        logbook = tools.Logbook()
        logbook.header = ["gen", "evals"] + stats.fields
        GEN = 10000
        best = None
        for g in range(GEN):
            for part in pop:
                part.fitness.values = toolbox.evaluate(part)
                if not part.best or part.best.fitness < part.fitness:
                    part.best = creator.Particle(part)
                    part.best.fitness.values = part.fitness.values
                if not best or best.fitness < part.fitness:
                    best = creator.Particle(part)
                    best.fitness.values = part.fitness.values
            
            for part in pop:
                toolbox.update(part, best)
                # Gather all the fitnesses in one list and print the stats
            logbook.record(gen=g, evals=len(pop), **stats.compile(pop))
                #print(logbook.stream)
                #print ("  Best so far: %s - %s" % (best, best.fitness))
        return pop, logbook, best
    pop,logbook,best= main()
    a=np.array(best)
    b=10+a[0]**2+a[1]**2-10*(np.cos(2*np.pi*a[0])+np.cos(2*np.pi*a[1]))
    tiempo = time.time() - start_time;
    resultados=(b,a[0],a[1],tiempo)
    
    return (resultados)
if __name__ == "__main__":
    nucleos_del_computador=cpu_count()
    p = Pool(processes=nucleos_del_computador)
    result = p.map(jacobo_genetico,range(100))#np.array(p.map(f,range(100)))
    #print(result)
    alpha=np.array(result)
    
    workbook = xlsxwriter.Workbook('AlgoritmoGenetico10000hijos.xlsx')
    worksheet = workbook.add_worksheet(name='datos')
    row_count = 0
    column_count = 0
    table_headers = [
            {'header': 'f(x,y)'},
            {'header': 'x'},
            {'header': 'y'},
            {'header': 'tiempo'}]
    excel_write_data = alpha
    table_row_count = row_count + len(excel_write_data)
    table_column_count = column_count + len(table_headers)-1
    table1=worksheet.add_table(row_count, column_count,
                        table_row_count, table_column_count,
                        {'data': excel_write_data,
                         'columns': table_headers})

    worksheet.write('A1', 'f(x,y)')
    worksheet.write('B1', 'X')
    worksheet.write('C1', 'Y')
    worksheet.write('D1', 'Tiempo')
    worksheet.write('F1', 'Tiempo Total ejecucion')
    worksheet.write_formula('F2', '=_xlfn.SUM(D:D)')
    worksheet.write('F3', 'Desviacion Estandar f(x,y)')
    worksheet.write_formula('F4', '=_xlfn.STDEV.P(A:A)')
    worksheet.write('F5', 'Desviacion Estandar X')
    worksheet.write_formula('F6', '=_xlfn.STDEV.P(B:B)')
    worksheet.write('F7', 'Desviacion Estandar Y')
    worksheet.write_formula('F8', '=_xlfn.STDEV.P(C:C)')
    worksheet.write('I2', 'f(x,y)')
    worksheet.write('I3', 'X')
    worksheet.write('I4', 'Y')
    worksheet.write('J1', 'MINIMO')
    worksheet.write_formula('J2', '=_xlfn.MIN(A:A)')
    workbook.close()