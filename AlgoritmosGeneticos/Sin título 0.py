# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 19:47:29 2017

@author: Family
"""

from funciones import PSO

def funFitness(x):
    total=10+x[0]**2+x[1]**2-10*(np.cos(2*np.pi*x[0])+np.cos(2*np.pi*x[1]))
    return total

a=PSO(funFitness,x0=[6,4],limites=[(-10,10),(-10,10)],num_particulas=100,iteracionesmax=100)